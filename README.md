# Installation de Drupal

## Prérequis
Installer la dernière version de nodejs, gulp, foundation et la librairie php gd
```
sudo npm install n -g
sudo n stable
npm install -g gulp
npm install --global foundation-cli
sudo apt-get install php5-gd
```

## Installation de composer 
Si ce n'est pas encore installé
```
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo chown USER:USER /usr/local/bin/composer

```

#installation de npm
Si ce n'est pas encore installé
```
sudo apt-get install npm
```


## Configuration php
Si ce n'est pas encore installé
```
sudo apt-get install php5-curl
```


## Vhost
* Créer un vhost, par exemple bs-drupal8.ch ( On peut utiliser apIgniter, mais attention de remettre le bon propriétaire sur les dosier et de vider totalement le dossier du vhost)

## Installation de drupal
Se placer dans le /var/www/bs-drupal8.ch

```
git clone https://bluesys@bitbucket.org/bluesys/bs-drupal8.ch.git ./
composer update
cd web/sites/default
rm settings.php
cp default.settings.php settings.php
chmod 0777 settings.php
mkdir files
chmod 0777 files
```


## Installation du thème 
```
cd /var/www/bs-drupal8.ch/web/themes/bluesys01
npm install gulp
npm install gulp-load-plugins
npm install gulp-sass
>> si erreur : sudo apt-get install nodejs-legacy
npm install gulp-autoprefixer
foundation update
```

## Configuration de drupal
* local.bs-drupal8.ch
* Choisir la langue anglaise
* Choisir la version standard
* Introduire le nom de la base de donnée désiréer et les accès root à mysql (ou créer la base de données et mettre son utilisateur)
* Attention de bien mettre le prefix drupal_

Pour compiler Sass : 
```
foundation watch
```

et voilà