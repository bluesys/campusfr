(function ($) {

		$(document).foundation();

	

	$( "[data-slick-aside]" ).slick({
		arrows: false,
		fade: true,
		asNavFor: "[data-slick-main]"
	});

	$( "[data-slick-main]" ).slick({
		asNavFor: "[data-slick-aside]"
	});

	$( "[data-slick-news]" ).slick({
		dots: true,
		adaptiveHeight: true
	});
	$( "[data-slick-listing]" ).on('init', function(event, slick){
  		Foundation.reInit('equalizer')
	});
	$( "[data-slick-listing]" ).slick();

	$(document).ready(function(){
  		$('.message-action').delay(4000).animate({
		  	top: -100
		});
  	});

	//TEMPO
	$(document).ready(function() {
	    $(".language-switcher-language-url li[hreflang='fr'] a").text("FR");
	    $(".language-switcher-language-url li[hreflang='de'] a").text("DE");
	}); 
})(jQuery);